import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/first';

@Injectable()
export class ProcessService {

  constructor(private http: HttpClient) {}

  getTopLevel() {
    return this.http.get<any[]>('https://swe-nrb.gitlab.io/sbp-processer/assets/top-level.json');
  }

  getProcesses() {
    return this.http.get<Observable<any[]>>('https://swe-nrb.gitlab.io/sbp-processer/assets/populated-collection.json');
  }

  getProcess(slug) {
    return this.http
      .get<any[]>('https://swe-nrb.gitlab.io/sbp-processer/assets/populated-collection.json')
      .mergeMap(item => item)
      .first(item => {
        return item.slug == slug
      });
  }

}
