import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ProcessService } from '../process.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  process: Observable<any>;
  constructor(private processService: ProcessService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route
      .paramMap
      .switchMap(params => {
        return this.processService.getProcess(params.get('slug'));
      })
      .subscribe(item => {
        this.process = item
      });
  }

  goUpOneLevel() {
    this.router.navigate(['../']);
  }

  goToProcess(slug) {
    this.router.navigate([`/detail/${slug}`]);
  }

}
