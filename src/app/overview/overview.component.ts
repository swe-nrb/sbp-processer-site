import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ProcessService } from '../process.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {
  processes: Observable<any[]>;
  constructor(private processService: ProcessService) { }

  ngOnInit() {
    this.processes = this.processService.getTopLevel();
  }

}
